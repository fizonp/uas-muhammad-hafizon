document.getElementById('loginForm').addEventListener('submit', function(e) {
    e.preventDefault(); // Prevent form submission
  
    // Get form values
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
  
    // Check if username and password are not empty
    if (username.trim() !== '' && password.trim() !== '') {
      // Perform login logic here
      // For demonstration purposes, we'll just show a success message
      document.getElementById('error').textContent = '';
      alert('Login successful!');
      window.location.href = "index.html";
    } else {
      // Show error message if fields are empty
      document.getElementById('error').textContent = 'Please enter username and password.';
    }
  });

  
function tampilkanKontak() {
    var kontak = {
        nama: "Muhammad Hafizon",
        email: "muhammadhafizon345@gmail.com",
        telepon: "+62897542467"
    };

    var pesan = "Silakan hubungi kami melalui:\n" +
        "Nama: " + kontak.nama + "\n" +
        "Email: " + kontak.email + "\n" +
        "Telepon: " + kontak.telepon;

    alert(pesan);
}

