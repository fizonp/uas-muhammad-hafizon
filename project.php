<!DOCTYPE html>
<html>
<head>
  <title>Data Riwayat Hidup</title>
  <style>
    body {
        background: #a430f1;
        margin: 0;
    }
    .container {
      max-width: 600px;
      margin: 10px 530px;
      padding: 20px;
      background-color: #f2f2f2;
      border-radius: 5px;
    }

    h1 {
      text-align: center;
    }

    .list {
      list-style-type: none;
      padding: 0;
      margin: 20px 0;
    }

    .list li {
      background-color: #fff;
      padding: 10px;
      margin-bottom: 10px;
      border-radius: 5px;
    }
  </style>
</head>
<body>
  <div class="container">
    <h1>Data Riwayat Hidup</h1>

    <ul class="list">
      <?php
      $mahasiswa = array(
        array("nama" => "Project 480",  "waktu" => "2021"),
        array("nama" => "Project KINFOLK", "waktu" => "2021"),
        array("nama" => "Project AFK Pelalawan", "waktu" => "2021"),
        array("nama" => "Project HIPMI Pelalawan", "waktu" => "2022"),
        array("nama" => "Project Futsal Smansa ", "waktu" => "2022"),
        array("nama" => "Project Futsal Bernas Cup ", "waktu" => "2022"),
        array("nama" => "Project Piala Gubernur MLBB Cup ", "waktu" => "2022"),
        array("nama" => "Project 480 Esi Pelalawan ", "waktu" => "2022"),
        array("nama" => "Project Futsal Bernas Cup ", "waktu" => "2022"),
        array("nama" => "Project Pelalawan Emas ", "waktu" => "2022"),
        array("nama" => "Project Tanjak Studio ", "waktu" => "2022"),
        array("nama" => "Project Wira The Goat March ", "waktu" => "2022"),
        array("nama" => "Project Mandi Belimau IPMR-KP ", "waktu" => "2023"),
        array("nama" => "Project Makrab KBM Riau UAD ", "waktu" => "2023"),
        array("nama" => "Project KBM Riau UAD MLBB CUP ", "waktu" => "2023"),
        array("nama" => "Project Arizal Cup", "waktu" => "2023"),
        array("nama" => "Project EXE ", "waktu" => "2023"),
        array("nama" => "Project Del Eury ", "waktu" => "2023")

      );
      for ($i = 0; $i < count($mahasiswa); $i++) {
        echo "<li>";
        echo "<h3>" . $mahasiswa[$i]['nama'] . "</h3>";
        echo  "<p>Tahun: ".$mahasiswa[$i]['waktu'] . "</p>";
        echo "</li>";
      }
      ?>
    </ul>
  </div>
</body>
</html>
